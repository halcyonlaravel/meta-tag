<?php

return [
    'media' => [
        'collection_name' => 'image',
        'conversion_name' => 'resized-optimized',
        'register_conversions' => HalcyonLaravelBoilerplate\MetaTag\Configurations\Conversion\Conversion::class,
    ],
];
