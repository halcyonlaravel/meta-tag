# Release Notes for 3.x

    2021-09-14 v3.2.3: Add morphTo relation in model.
    2021-04-23 v3.2.2: Add support for PHP8
    2020-11-04 v3.2.1: Remove medialibrary in require, already in media-library-uploader
    2020-09-09 v3.2.0: Add support for laravel 8 
    2020-09-05 v3.1.0: Fix config to able config serialise
    2020-07-21|2020-07-23 v3.0.0: set minimum PHP 7.4