# Release Notes for 1.x

    2020-03-12 v1.0.14: Fix psr-4
    2020-03-10 v1.0.10: Manage Change log file
    2020-03-05 v1.0.9: Fix Add/Update author
    2020-03-05 v1.0.7: Fix Create vs Update
    2020-03-05 v1.0.6: Add image
    2020-03-05 v1.0.5: Add strip_tags
    2020-03-05 v1.0.4: Add Media Library Uploader, Add media collection name in config
    2020-03-05 v1.0.2: Add Fillable
    2020-03-05 v1.0.1: Add toArray
    2020-02-11 v1.0.0: Initial release