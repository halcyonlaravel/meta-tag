@include('meta::manager', [
'title'         => $title,
'description'   => $description,
'keywords' => $keywords,
'image'         => $image,
'author' => $author,
])
