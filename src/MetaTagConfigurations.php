<?php

namespace HalcyonLaravelBoilerplate\MetaTag;

use HalcyonLaravelBoilerplate\MetaTag\Configurations\MetaTagValues;

/**
 * Class MetaTagConfigurations
 *
 * @package HalcyonLaravelBoilerplate\MetaTag
 */
class MetaTagConfigurations
{
    public ?MetaTagValues $values;

    /**
     * @return $this
     */
    public static function create(): self
    {
        return (new static())->reset();
    }

    /**
     * @return $this
     */
    public function reset(): self
    {
        $this->values = null;

        return $this;
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\MetaTag\Configurations\MetaTagValues  $values
     *
     * @return $this
     */
    public function values(MetaTagValues $values): self
    {
        $this->values = $values;

        return $this;
    }
}
