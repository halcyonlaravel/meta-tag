<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Facades;

use HalcyonLaravelBoilerplate\MetaTag\MataTagManager;
use Illuminate\Support\Facades\Facade;

/**
 * Class MetaTagFacade
 *
 * @mixin \HalcyonLaravelBoilerplate\MetaTag\MataTagManager
 * @package HalcyonLaravelBoilerplate\MetaTag\Facades
 */
class MetaTagFacade extends Facade
{
    public static function getFacadeAccessor()
    {
        return MataTagManager::class;
    }
}
