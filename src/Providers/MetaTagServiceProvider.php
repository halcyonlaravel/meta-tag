<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Providers;

use HalcyonLaravelBoilerplate\MetaTag\MataTagManager;
use Illuminate\Support\ServiceProvider;

class MetaTagServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes(
            [
                __DIR__.'/../../config/meta-tag.php' => config_path('meta-tag.php'),
            ],
            'config'
        );

        if (!class_exists('CreateMetaTagsTable')) {
            $this->publishes(
                [
                    __DIR__.'/../../database/migrations/create_meta_tags_table.php.stub' => database_path(
                        'migrations/'.date(
                            'Y_m_d_His',
                            time()
                        ).'_create_meta_tags_table.php'
                    ),
                ],
                'migrations'
            );
        }

        $this->publishes(
            [
                __DIR__.'/../../resources/views' => resource_path('views/vendor/metaTag'),
            ],
            'views'
        );

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'metaTag');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/meta-tag.php', 'meta-tag');

        $this->app->singleton(MataTagManager::class);
    }

}
