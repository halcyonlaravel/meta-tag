<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Models\Traits;

use HalcyonLaravelBoilerplate\MediaLibraryUploader\MediaLibraryUploader;
use HalcyonLaravelBoilerplate\MetaTag\Facades\MetaTagFacade;
use HalcyonLaravelBoilerplate\MetaTag\MetaTagConfigurations;
use HalcyonLaravelBoilerplate\MetaTag\Models\MetaTag;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Http\UploadedFile;

trait HasMetaTags
{
    /**
     * @return \HalcyonLaravelBoilerplate\MetaTag\MetaTagConfigurations
     */
    abstract public function metaTagConfigurations(): MetaTagConfigurations;

    /**
     * @param  string|null  $title
     * @param  string|null  $description
     * @param  string|null  $keywords
     * @param  string|null  $author
     * @param  \Illuminate\Http\UploadedFile|null  $uploadedFile
     *
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function saveMetaValueFromModel(
        string $title = null,
        string $description = null,
        string $keywords = null,
        string $author = null,
        UploadedFile $uploadedFile = null
    ): void {
        $metaTagValues = $this->metaTagConfigurations()->values;
        $data = [
            'title' => $title ?: $metaTagValues->title,
            'description' => strip_tags($description ?? $metaTagValues->description ?? ''),
            'keywords' => $keywords ?: $metaTagValues->keywords,
            'author' => $author ?: $metaTagValues->author,
        ];

        /** @var MetaTag $meta */
        if ($meta = $this->metaTag) {
            $meta->update($data);
        } else {
            $meta = $this->metaTag()->create($data);
        }

        if (blank($uploadedFile) && filled($file = optional($metaTagValues)->image)) {
            $uploadedFile = $file;
        }

        if (filled($uploadedFile)) {
            MediaLibraryUploader::setModel($meta)
                ->file($uploadedFile)
                ->execute(config('meta-tag.media.collection_name'), '');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function metaTag(): MorphOne
    {
        return $this->morphOne(MetaTag::class, 'model');
    }

    /**
     * return void
     */
    public function setModelAsMetaTags()
    {
        MetaTagFacade::setModel($this);
    }
}
