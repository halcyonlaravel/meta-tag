<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Models;

use HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Traits\MediaLibraryUploaderTrait;
use HalcyonLaravelBoilerplate\MetaTag\Configurations\Conversion\ConversionContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * HalcyonLaravelBoilerplate\MetaTag\Models\MetaTag
 *
 * @property int $id
 * @property string $model_type
 * @property int $model_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $author
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection $media
 * @property-read int|null $media_count
 * @method static Builder|MetaTag newModelQuery()
 * @method static Builder|MetaTag newQuery()
 * @method static Builder|MetaTag query()
 * @method static Builder|MetaTag whereAuthor($value)
 * @method static Builder|MetaTag whereCreatedAt($value)
 * @method static Builder|MetaTag whereDescription($value)
 * @method static Builder|MetaTag whereId($value)
 * @method static Builder|MetaTag whereKeywords($value)
 * @method static Builder|MetaTag whereModelId($value)
 * @method static Builder|MetaTag whereModelType($value)
 * @method static Builder|MetaTag whereTitle($value)
 * @method static Builder|MetaTag whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MetaTag extends Model implements HasMedia
{
    use MediaLibraryUploaderTrait;

    protected $fillable = [
        'title',
        'description',
        'keywords',
        'author',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(config('meta-tag.media.collection_name'))
            ->singleFile()
            ->registerMediaConversions(
                function (Media $media) {
                    $class = config('meta-tag.media.register_conversions');

                    $interfaces = class_implements($class);
                    if (!$interfaces || !in_array(ConversionContract::class, $interfaces)) {
                        abort(500, "Class `$class` must implement ".ConversionContract::class);
                    }

                    $class::run($this);
                }
            );
    }

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
