<?php

namespace HalcyonLaravelBoilerplate\MetaTag;

use HalcyonLaravelBoilerplate\MetaTag\Contracts\HasMetaTagInterface;
use InvalidArgumentException;

class MataTagManager
{
    private const VALID_TAGS = [
        'title_delimiter',
        'title_prefix',
        'title_suffix',
        'title',
        'description',
        'keywords',
        'author',
        'image',
    ];

    private const MODEL_ATTRIBUTES = [
        'title',
        'description',
        'keywords',
        'author',
        'image',
    ];

    private array $default = [];
    private string $template = 'metaTag::meta-tag';
    private array $tags = [];
    private ?HasMetaTagInterface $model = null;

    /**
     * @param  array  $args
     */
    private static function validateArguments(array $args)
    {
        if (blank($args)) {
            throw new InvalidArgumentException('Argument must not empty.');
        }

        foreach ($args as $arg => $v) {
            if (!in_array($arg, self::VALID_TAGS)) {
                throw new InvalidArgumentException("[$arg] is not valid argument.");
            }
        }
    }

    /**
     * @param  array  $defaultTags
     *
     * @return $this
     */
    public function setDefault(array $defaultTags = []): self
    {
        self::validateArguments($defaultTags);

        $this->default = array_merge($this->default, $defaultTags);

        return $this;
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\MetaTag\Contracts\HasMetaTagInterface  $model
     *
     * @return $this
     */
    public function setModel(HasMetaTagInterface $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function toArray(array $extra = [])
    {
        return $extra + [
                'title' => $this->parseTitle(),
                'description' => $this->getTag('description'),
                'keywords' => $this->getTag('keywords'),
                'image' => $this->getTag('image'),
                'author' => $this->getTag('author'),
            ];
    }

    /**
     * @return array|string
     * @throws \Throwable
     */
    public function render()
    {
        return view(
            $this->template,
            $this->toArray()
        )->render();
    }

    /**
     * @param  array  $tags
     *
     * @return $this
     */
    public function setTags(array $tags = []): self
    {
        self::validateArguments($tags);

        $this->tags = array_merge($this->tags, $tags);

        return $this;
    }

    private function parseTitle(): string
    {
        $d = $this->getTag('title_delimiter', ' - ');
        $t = $this->getTag('title');

        $p = $this->getTag('title_prefix');
        $p = !blank($p) && $p != $t ? $p.$d : '';

        $s = $this->getTag('title_suffix');
        $s = !blank($s) && $s != $t ? $d.$s : '';

        return $p.$t.$s;
    }

    /**
     * @param  string  $tagName
     * @param  string|null  $default
     *
     * @return mixed|string
     */
    private function getTag(string $tagName, string $default = null)
    {
        if (
            in_array($tagName, self::MODEL_ATTRIBUTES) &&
            filled($this->model) &&
            $metaTag = optional($this->model)->metaTag
        ) {
            if ($tagName == 'image') {
                /** @var \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media $media */
                if (filled($media = $metaTag->getFirstMedia(config('meta-tag.media.collection_name')))) {
                    $result = $media
                        ->getFullUrl(config('meta-tag.media.conversion_name'));
                } else {
                    return null;
                }
            } else {
                $result = $metaTag->{$tagName};
            }

            if (filled($result)) {
                return $result;
            }
        }

        if (isset($this->tags[$tagName]) && filled($this->tags[$tagName])) {
            $result = $this->tags[$tagName];
            if (filled($result)) {
                return $result;
            }
        }

        if (isset($this->default[$tagName]) && filled($this->default[$tagName])) {
            $result = $this->default[$tagName];
            if (filled($result)) {
                return $result;
            }
        }
        return $default;
    }
}
