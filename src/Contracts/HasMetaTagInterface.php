<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Http\UploadedFile;

interface HasMetaTagInterface
{

    /**
     * return void
     */
    public function setModelAsMetaTags();

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function metaTag(): MorphOne;

    /**
     * @param  string|null  $title
     * @param  string|null  $description
     * @param  string|null  $keywords
     * @param  string|null  $author
     * @param  \Illuminate\Http\UploadedFile|null  $uploadedFile
     */
    public function saveMetaValueFromModel(
        string $title = null,
        string $description = null,
        string $keywords = null,
        string $author = null,
        UploadedFile $uploadedFile = null
    ): void;
}
