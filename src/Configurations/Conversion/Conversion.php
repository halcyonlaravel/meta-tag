<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Configurations\Conversion;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;

class Conversion implements ConversionContract
{
    /**
     * @param  \Spatie\MediaLibrary\HasMedia  $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public static function run(HasMedia $media): void
    {
        $media->addMediaConversion('resized-optimized')
            ->fit(Manipulations::FIT_CROP, 700, 700);
    }

}
