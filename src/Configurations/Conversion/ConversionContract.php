<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Configurations\Conversion;

use Spatie\MediaLibrary\HasMedia;

interface ConversionContract
{
    public static function run(HasMedia $media): void;
}