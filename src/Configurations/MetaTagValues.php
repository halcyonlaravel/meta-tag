<?php

namespace HalcyonLaravelBoilerplate\MetaTag\Configurations;

class MetaTagValues
{
    public ?string $title;
    public ?string $keywords;
    public ?string $description;
    public ?string $image;
    public ?string $author;

    /**
     * @return $this
     */
    public static function create(): self
    {
        return (new static())->reset();
    }

    /**
     * @return $this
     */
    public function reset(): self
    {
        $this->title = null;
        $this->description = null;
        $this->keywords = null;
        $this->image = null;
        $this->author = null;

        return $this;
    }

    /**
     * @param  string  $title
     *
     * @return $this
     */
    public function title(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param  string  $description
     *
     * @return $this
     */
    public function description(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param  string  $keywords
     *
     * @return $this
     */
    public function keywords(string $keywords): self
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @param  string  $uploadedFile
     *
     * @return $this
     */
    public function image(?string $uploadedFile): self
    {
        $this->image = $uploadedFile;
        return $this;
    }

    /**
     * @param  string  $author
     *
     * @return $this
     */
    public function author(string $author): self
    {
        $this->author = $author;
        return $this;
    }
}
