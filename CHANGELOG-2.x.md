# Release Notes for 2.x

    2020-05-05 v2.0.3: Fix nullable image media collection
    2020-04-07 v2.0.2: Fix/Enhance saveMetaValueFromModel()
    2020-03-24 v2.0.1: Version for merge branch 1.x
    2020-03-10 v2.0.0: Prepare for Laravel 7